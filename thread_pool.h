#ifndef thread_pool_
#define thread_pool_ 
#include <thread>
#include <functional>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <future>
#include <vector>
#include <atomic>
#include <type_traits>
#include <iostream>
#include <stdexcept>

using function_t = std::function<void()>;

std::mutex mtx_;
std::condition_variable cv_;
std::atomic<bool> stopped_{false};

class task_queue {
	public:
		bool search_for_task(function_t& fun) {
			std::unique_lock<std::mutex> queue_lock{mtx_};
			if ( tasks_.empty() ) {
				if ( stopped_ ) return false;
				fun = nullptr;
			} else {
				fun = std::move(tasks_.front());
				tasks_.pop();
			}
			return true;
		}

		bool sleep_while_empty(){
			std::unique_lock<std::mutex> queue_lock{mtx_};
			while ( tasks_.empty() ) {
				if ( stopped_ ) return false;
				cv_.wait(queue_lock);
			}
			return true;
		}

		template<typename F>
			void push(F&& f) {
				{
					std::unique_lock<std::mutex> queue_lock{mtx_};
					tasks_.emplace(std::forward<F>(f));
				}
				cv_.notify_one();
			}

		void stop() {
			{
				std::unique_lock<std::mutex> queue_lock{mtx_};
				stopped_ = true;
			}
			cv_.notify_all();
		}

	private:
		std::queue<function_t> tasks_;
};

class thread_pool {
	public:
		thread_pool(size_t th_num = std::thread::hardware_concurrency()) : thread_number_{th_num}, queue_number{0} {
				tasks_.assign(thread_number_, task_queue());
				for(int i = 0; i < thread_number_; ++i){
					threads_.emplace_back( std::thread{ [this, i](){ run(i); }});
				}
		}

		~thread_pool() {
			for(int i = 0; i < thread_number_; ++i)
				tasks_[i].stop();
			
			for(auto &t : threads_ ) 
				t.join();
		}

		template<typename T>
			void async(T&& fun) {
				tasks_[queue_number].push(std::forward<T>(fun));
				queue_number++;
				if(queue_number == thread_number_) queue_number = 0;
			}

	private:
		void run(int i) {
			while(true) {
				function_t fun;
				if( !tasks_[i].search_for_task(fun) )
					return;
				int j = 0;
				while(fun == nullptr && j != thread_number_){
						if( i == j ) {
							++j;
							continue;
						}
						if( !tasks_[j].search_for_task(fun) )
							return;
						++j;
				}
				if( j == thread_number_ && fun == nullptr) tasks_[i].sleep_while_empty();
				else if ( fun != nullptr ) {
					fun();
				}
			}
		}

		const size_t thread_number_;
		std::vector<std::thread> threads_;
		std::vector<task_queue> tasks_;
		std::atomic<int> queue_number;
};

#endif /* ifndef thread_pool_ */

